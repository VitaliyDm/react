import React, { Component } from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import { hot } from 'react-hot-loader';
import { Provider } from 'react-redux';
import { render } from 'react-dom';

import '~/base.styl';
import * as style from './style.styl';
import { history, store } from './store';

import { userInf } from './effects/auth'

import { MainRoute, ProfileRoute, CalendarRoute, ToDoListRoute } from './routes';

class AppComponent extends Component {
    state = {
        routes: [{
            component: MainRoute,
            exact: true,
            icon: 'home',
            link: '/',
            title: 'Main Route',
        }, {
            component: ProfileRoute,
            exact: false,
            icon: 'home',
            link: '/profile',
            title: 'Profile Route',
        },{
            component: CalendarRoute,
            exact: false,
            icon: 'home',
            link: '/calendar',
            title: 'Calendar Route',
        },{
            component: ToDoListRoute,
            exact: false,
            icon: 'home',
            link: '/todolist',
            title: 'ToDoList Route',
        }],

    }

    render() {
        const { routes } = this.state;
        return (
            <div className={style.container}>
                <Switch>
                    {routes.map(route => <Route key={route.link}
                        exact={route.exact}
                        path={route.link}
                        component={route.component}
                    />)}
                    <Redirect to="/" />
                </Switch>
            </div>
        );
    }
}

const App = hot(module)(AppComponent);

export function bootApp(user){
    console.log("this is userInf = " + userInf);
    render(
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <Router>
                    <App />
                </Router>
            </ConnectedRouter>
        </Provider>,
        document.getElementById('root')
    );
}
