import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import options from '`/options.json';
import { Link } from 'react-router-dom'

import { checkEmail } from '@/effects/auth';
import style from './style.styl';

import exitImg from '^/exit.svg'
import backImg from '^/back.svg'
import calendarImg from '^/calendar.svg'
import profileImg from '^/profile.svg'

@connect(({ user }) => ({ user }))
export default class ProfileRoute extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        user: PropTypes.object.isRequired,
    }

    handleExit(){
        localStorage.removeItem(options.LOGINTOKEN);
        window.location.href = '/';
    }

    render() {
        const { user } = this.props;
        console.log(user);
        return (
             <div className={style.container}>
                <div className={style.navMenu}>
                   <Link to='/' className={style.backButton}>
                       <img src={backImg} alt='go_back_img' />
                   </Link>
                   <div className={style.exitButton} onClick={this.handleExit}>
                       <img src={exitImg} alt='exit_img' />
                       <p>Выйти</p>
                   </div>
                </div>
                <div className={style.profileMenu}>
                    <p>Настройки профиля</p>
                    <img src={profileImg} className={style.userImg} alt="profile_img" />
                    <button className={style.imgLoadButton + ' ' + style.profileButton}>Загрузить</button>
                    <form className={style.profileChangesForm}>
                        <input className={style.nameField} placeholder="Введите имя"/>
                        <div className={style.birthDateContainer}>
                            <input className={style.birthDateInput} placeholder='Введите дату рождения'/>
                            <img className={style.birthDateImg} src={calendarImg} alt="birth_date_img" />
                        </div>
                        <button className={style.profileButton}>Сохранить</button>
                    </form>
                    <div className={style.userSessionsContainer}>
                        <p>Ваши сессии</p>
                        <div className={style.userSessions}>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
