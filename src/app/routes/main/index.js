import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import options from '`/options.json';
import {Link} from 'react-router-dom'

import profileImg from '^/profile.svg';
import calendarImg from '^/calendar.svg';
import todolistImg from '^/todolist.svg';
import exitImg from '^/exit.svg';
import icons from '^/icomoon.svg'

import { checkEmail } from '@/effects/auth';
import style from './style.styl';

@connect(({ user }) => ({ user }))
export default class MainRoute extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        user: PropTypes.object.isRequired,
    }

    handleExit(){
        localStorage.removeItem(options.LOGINTOKEN);
        window.location.href = '/';
    }

    render() {
        const { user } = this.props;

        return (
            <div className={style.container}>
                <div className={style.exitButton} onClick={this.handleExit}>
                    <img src={exitImg} alt='exit_img' />
                    <p>Выйти</p>
                </div>
                <div className={style.mainMenu}>
                    <Link to='/profile' className={style.menuBlock}>
                        <img src={profileImg} alt='Profile_img' className={style.menuImg}/>
                        <p>Профиль</p>
                    </Link>

                    <Link to='/todolist' className={style.menuBlock}>
                        <img src={todolistImg} alt='ToDoList_img' className={style.menuImg}/>
                        <p>Дела</p>
                    </Link>
                    <Link to='/calendar' className={style.menuBlock}>
                        <img src={calendarImg} alt='Calendar_img' className={style.menuImg}/>
                        <p>Календарь</p>
                    </Link>
                </div>
            </div>
        );
    }
}
