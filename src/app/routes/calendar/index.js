import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import options from '`/options.json';
import { Link } from 'react-router-dom'
import moment from 'moment'

import { checkEmail } from '@/effects/auth';
import style from './style.styl';

import exitImg from '^/exit.svg'
import backImg from '^/back.svg'

@connect(({ user }) => ({ user }))
export default class CalendarRoute extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        user: PropTypes.object.isRequired,
    }

    state = {
        month: moment().month(),
        year: moment().year(),
        calendar: this.getCalendar(moment().month(), moment().year()),
    }

    handleExit(){
        localStorage.removeItem(options.LOGINTOKEN);
        window.location.href = '/';
    }

    getCalendar(month, year){
        moment.locale('ru');
        const startDate = moment([year, month]);
        const startDay = moment(startDate).startOf('month').startOf('week');
        const endDay = moment(startDate).endOf('month').endOf('week');

        let monthCalendar = []
        var index = startDay.clone().subtract(1, 'day');
        while (index.isBefore(endDay, 'day')) {
            monthCalendar.push(
                new Array(7).fill(0).map(
                    function(n, i) {
                        return {
                            date: index.add(1, 'day').date(),
                            active: index.month() == month,
                        };
                    }
                )
            );
        }

        return monthCalendar;
    }

    getNextMonth = evevnt => {
        evevnt.preventDefault();
        const {month, year} = this.state;
        let changedMonth = month;
        let changedYear = year;

        if (month == 11){
            changedMonth = 0;
            changedYear = year + 1;
        }
        else{
            changedMonth = month + 1;
            changedYear = year;
        }

        this.setState({
            month: changedMonth,
            year: changedYear,
            calendar: this.getCalendar(changedMonth, changedYear),
        })
    }

    getPreviousMonth = event => {
        event.preventDefault();
        const {month, year} = this.state;
        let changedMonth = month;
        let changedYear = year;

        if (month == 0){
            changedMonth = 11;
            changedYear = year - 1;
        }
        else {
            changedMonth = month - 1;
            changedYear = year;
        }

        this.setState({
            month: changedMonth,
            year: changedYear,
            calendar: this.getCalendar(changedMonth, changedYear),
        })

    }

    render() {
        const {year , month, calendar} = this.state;
        const { user } = this.props;

        console.log(month)

        return (
            <div className={style.container}>
                <div className={style.navMenu}>
                   <Link to='/' className={style.backButton}>
                       <img src={backImg} alt='go_back_img' />
                   </Link>
                   <div className={style.exitButton} onClick={this.handleExit}>
                       <img src={exitImg} alt='exit_img' />
                       <p>Выйти</p>
                   </div>
                </div>
                <div className={style.calendarContainer}>
                    <p>Календарь</p>
                    <p onClick={this.getPreviousMonth} className={style.calendarNav}>&lt;</p>
                    <p className={style.calendarNav + ' ' + style.currentMonth}>{moment([year, month]).format('MMMM YYYY')}</p>
                    <p onClick={this.getNextMonth} className={style.calendarNav}>&gt;</p>
                    <div className={style.weekDays}>
                        <p>пн</p>
                        <p>вт</p>
                        <p>ср</p>
                        <p>чт</p>
                        <p>пт</p>
                        <p>сб</p>
                        <p>вс</p>
                    </div>
                    <div className={style.calendar}>
                        {calendar.map((week) =>
                            week.map((day) =>
                                <div className={style.calendarDay + (day.active?'':' '+style.anotherMonth)} key={Math.random()}> {day.date} </div>))}
                    </div>
                </div>
            </div>
        );
    }
}
