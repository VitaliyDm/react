import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import options from '`/options.json';

import { checkEmail } from '@/effects/auth';
//import style from './style.styl';

@connect(({ user }) => ({ user }))
export default class ToDoListRoute extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        user: PropTypes.object.isRequired,
    }

    handleExit(){
        localStorage.removeItem(options.LOGINTOKEN);
        window.location.href = '/';
    }

    render() {
        const { user } = this.props;

        return (
            <div>
                ToDoList
            </div>
        );
    }
}
