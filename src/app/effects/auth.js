import { setAuthCredentialsAction } from '@/actions/auth';
import request from '@/services/request';

export const checkEmail = email => dispatch => request.get(`https://nodejs-express-starter.tk/auth/check/${email}`)
    .then(payload => {console.log(email); dispatch(setAuthCredentialsAction(payload))});

export const userInf = request.get('http://localhost:5000/user', {}, {"authorization":"44b332a698f54765381999a9c912d026516740c6b1e53813ca9b7a715e5bee5a"})
                        .then(payload => console.log("payload " + payload))
