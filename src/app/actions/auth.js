import { createAction } from 'redux-act';

export const setAuthCredentialsAction = createAction('set user credentials after checking email');
