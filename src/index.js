import 'babel-polyfill';
import axios from 'axios';
import options from '`/options.json'

const HOST = process.env.NODE_ENV == 'production'
    ? options.HOST.PRODUCTION
    : options.HOST.DEV;

console.log(HOST);
console.log(axios.get(HOST + '/user', { headers: { authorization: localStorage.getItem('app_token') }}));
axios.get(HOST + '/user', { headers: { authorization: localStorage.getItem('app_token') }})
    .then(res => {
        import('./app')
            .then(App => App.bootApp())
    })
    .catch(() => {
        console.log(localStorage.getItem(options.LOGINTOKEN))
        import('./login')
            .then(App => App.bootApp())
    });
