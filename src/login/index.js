import React, { Component } from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import { hot } from 'react-hot-loader';
import { Provider } from 'react-redux';
import { render } from 'react-dom';

import '~/base.styl';
import * as style from './style.styl';
import { history, store } from './store';

import { MainRoute, LoginRoute } from './routes';

class AppComponent extends Component {
    state = {
        routes: [{
            component: MainRoute,
            exact: true,
            icon: 'home',
            link: '/',
            title: 'Main Route',
        }, {
            component: LoginRoute,
            exact: false,
            icon: 'home',
            link: '/login',
            title: 'Login Route',
        }],
    }

    render() {
        const { routes } = this.state;
        return (
            <div className={style.container}>
                <Switch>
                    {routes.map(route => <Route key={route.link}
                        exact={route.exact}
                        path={route.link}
                        component={route.component}
                    />)}
                    <Redirect to="/" />
                </Switch>
            </div>
        );
    }
}

const App = hot(module)(AppComponent);

export function bootApp(user) {
    render(
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <Router>
                    <App />
                </Router>
            </ConnectedRouter>
        </Provider>,
        document.getElementById('root')
    );
}
