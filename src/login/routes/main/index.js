import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';

import { checkEmail } from '@/effects/auth';
import style from './style.styl';

@connect(({ user }) => ({ user }))
export default class MainRoute extends Component {
    render() {
        const { user } = this.props;

        return (
            <Link to='/login' className={style.container}>Войти</Link>
        );
    }
}
