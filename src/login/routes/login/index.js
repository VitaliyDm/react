import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import axios from 'axios/index';

import alertImg from '^/alert.svg'

import { checkEmail, correctLoginData, login } from '../../effects/auth';
import style from './style.styl';

@connect(({ user }) => ({ user }))
export default class LoginRoute extends Component{

    state = {
        email: '',
        password: '',
    }

    handleEmailInputChange = event => {
        const { value } = event.target;
        this.setState({ email: value });
    }

    handleEmailPasswordChange = event => {
        const { value } = event.target;
        this.setState({ password: value });
    }

    handleLogin = event => {
        console.log("logining")
        event.preventDefault();
        console.log(this);
        login(this.state)
            .then(() => {
                if (!correctLoginData){
                    this.setState({ email: '', password: '' })
                    console.log('next')
                    this.render();
                } else {
                    console.log("smth happend")
                    console.log(this.state.email)
                    console.log(typeof(checkEmail))
                    checkEmail(this.state.email)
                    //    .then(console.log("email checked"))
                }
            });

    }

	static propTypes =  {
		dispatch: PropTypes.func.isRequired,
		user: PropTypes.object.isRequired,
	}

	render() {
		const { user } = this.props;
        const { email, password } = this.state;

        if (correctLoginData)
            return (
                 <div className={style.container}>
                    <form id="login_form" onSubmit={this.handleLogin} className={style.loginform}>
                        <span className={style.title}>
                            Войти в приложение
                        </span>
                        <input type="text" placeholder="Введите логин" className={style.inputemail} onChange={this.handleEmailInputChange} name="email"/ >
                        <input type="password" placeholder="Введите пароль" className={style.inputpass} onChange={this.handleEmailPasswordChange} name="password"/>
                        <input type="submit" className={style.button} value="Войти"/>
                    </form>
                </div>
            );
        console.log('render2');
        console.log(email + password);
        return(
                <div className={style.container}>
                    <form id="login_form" onSubmit={this.handleLogin} className={style.loginform}>
                        <span className={style.title}>
                            Войти в приложение
                        </span>
                        <input type="text" placeholder="Введите логин" className={style.incorrectemail} onChange={this.handleEmailInputChange} value={email} name="email"/ >
                        <img src={alertImg} className={style.emailAlert} />
                        <input type="password" placeholder="Введите пароль" className={style.incorrectpass} onChange={this.handleEmailPasswordChange} value={password} name="password"/>
                        <img src={alertImg} className={style.passAlert} />
                        <input type="submit" className={style.button} value="Войти"/>
                    </form>
                </div>
        );

	}
}
