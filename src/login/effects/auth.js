import { setAuthCredentialsAction } from '@/actions/auth';
import request from '@/services/request';
import options from '`/options.json'
import axios from 'axios/index'

const HOST = process.env.NODE_ENV == 'production'
    ? options.HOST.PRODUCTION
    : options.HOST.DEV;

export const checkEmail = email => dispatch => request.get(`localhost:5000/auth/check/${email}`)
    .then(payload => dispatch(setAuthCredentialsAction(payload)));

export var correctLoginData = true

export const login = ({ password, email }) => {
    return axios.post(HOST + '/auth/in', { email, password })
        .then(response => {
            localStorage.setItem(options.LOGINTOKEN, response.data);
            window.location.href = '/';
        }, () => {correctLoginData = false; console.log(correctLoginData)});
}
