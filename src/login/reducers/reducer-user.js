import { createReducer } from 'redux-act';
import { setAuthCredentialsAction } from '@/actions/auth';

export default createReducer({
    [setAuthCredentialsAction]: (store, payload) => ({ ...store, ...payload }),
}, {
    email: '',
    icon: '',
    name: '',
});
