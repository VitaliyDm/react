# react-redux-starter
![Build Status](https://drone.dayler.io/api/badges/iknpx/react-redux-starter/status.svg)

[Example](https://react-redux-starter.tk)

### Stack
* [React](https://reactjs.org) / [Redux](https://redux.js.org)
* [Webpack](https://webpack.js.org)
* [Stylus](http://stylus-lang.com/) / [Rupture](http://jescalan.github.io/rupture)
* [ESLint](https://eslint.org)
* [Jest](https://facebook.github.io/jest) / [Enzyme](http://airbnb.io/enzyme)
* [Drone CI](https://drone.io/)
* [Moment](https://momentjs.com)

### Description
* Calendar which consist users ToDoList and their profile.
* Registration of new users is also supported

### Quick start
* Clone this repo using `git clone https://gitlab.com/VitaliyDm/react.git`
* Move to the appropriate directory: cd react-redux-starter.
* Run `yarn` or `npm install` to install dependencies.
* Run `npm start` to see the example app at http://localhost:8000.

### Run Tests
* `npm test` - default
* `npm test -- -u` - clear snapshots
* `npm test -- -coverage` - coverage

You can `coverageThreshold` props in `package.json` as you need.

### License
React-starter:
    MIT license, Copyright (c) 2018 Iakov Salikov. [github.com/iknpx](https://github.com/iknpx)
